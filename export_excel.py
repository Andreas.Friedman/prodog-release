#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 08:05:13 2024

@author: pi
"""

import importlib.util
import os, sys
sys.path.insert(0, os.environ["HOME"]+"/prodog/RaspberryPi/")


def install_package(package_name):
    try:
        importlib.import_module(package_name)
    except ImportError:
        print(f"Package '{package_name}' not found. Installing...")
        try:
            from pip._internal import main as pip_main
        except ImportError:
            from pip import main as pip_main
        pip_main(['install', package_name])
        importlib.invalidate_caches()  # Invalidate the import caches to reflect the newly installed package
package_name = "openpyxl"
install_package(package_name)

# Load the package
try:
    module = importlib.import_module(package_name)
    globals()[package_name] = module  # Add the loaded module to globals()
    print(f"Package '{package_name}' loaded successfully.")
except ImportError as e:
    print(f"Failed to load package '{package_name}': {e}")

#import openpyxl
from openpyxl.styles import Alignment
      
import prodog_db

def Einnahmen_exportieren(conn, start_timestamp):
    
    from datetime import datetime

    cur = conn.cursor()
    cur.execute("SELECT * FROM session WHERE start_timestamp > %s ORDER by start_timestamp ASC" % (start_timestamp or 0))
    rows = cur.fetchall()
    #print(rows)

    
    # Assuming 'rows' contains the result of your query
    # 'rows' should be a list of tuples where each tuple represents a row from the database
    
    # Specify the file name for the Excel output
    excel_file = os.environ["HOME"]+"/Desktop/Einnahmen %s.xlsx" % datetime.fromtimestamp(start_timestamp).strftime('%d.%m.%Y')
    
    # Create a new Excel workbook and select the active sheet
    wb = openpyxl.Workbook()
    ws = wb.active
    
    # Write the header row based on the column names in the database
    header = [i[0] for i in cur.description]
    ws.append(header)

    last_start_timestamp = None

    # Write the data rows
    for row in rows:
        # Convert the timestamp to a human-readable format (German localization)
        row_data = list(row)

        if row_data[1] == last_start_timestamp:
            continue
        last_start_timestamp = row_data[1]

        row_data[1] = datetime.fromtimestamp(row_data[1]).strftime('%d.%m.%Y %H:%M:%S')  # Assuming start_timestamp is at index 1
        row_data[2] = datetime.fromtimestamp(row_data[2]).strftime('%d.%m.%Y %H:%M:%S')  # Assuming end_timestamp is at index 1
        
        # Write the row data to the Excel sheet
        ws.append(row_data)
    
    # Auto-adjust column widths for better readability
    for col in ws.columns:
        max_length = 0
        for cell in col:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = (max_length + 2) * 1.2
        ws.column_dimensions[col[0].column_letter].width = adjusted_width
    
    # Save the Excel file
    wb.save(excel_file)
    
    print("Excel file generated successfully: " + excel_file)

    


def Transaktionen_exportieren(conn):
    try:
        start_timestamp = sys.argv[2]        
    except:
        start_timestamp = 0

    Einnahmen_exportieren(conn, start_timestamp)


if __name__ == "__main__":

    conn = prodog_db.create_db_connection()

    Transaktionen_exportieren(conn)


